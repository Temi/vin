<?php
?>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html"; charset="utf-8">
    <title>Galery - Tiles</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

<div id="galery">
<div id="header">
<h1>Tiles - Galery</h1>
<p id="authors">by Václav Stránský and Hana Brychtová</p>
   <a class="button" href="index.php">Back</a>

<div class="images">
    <?php
    $images = glob("../examples/*.png");

    foreach($images as $image) {
      echo "<img src=\"$image\" width=\"400px\" onclick=\"displayFull(this.src)\" />";
    }
    ?>
</div>

<img id="img_big" onclick="hideBigImg()"/>

<script>
    function displayFull(newsrc) {
        var img = document.getElementById("img_big");
        img.src = newsrc;
        img.style.display = "block";
        img.style.cursor = "pointer";
        img.style.top = window.pageYOffset;
    }

    function hideBigImg() {
        var img = document.getElementById("img_big");
        img.style.display = "none";
    }
</script>


</div>
</body>
</html> 
