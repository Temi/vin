<?php
# generovani razitka
function generateTile($width, $height, $shownShapes, $colormap, $thicknes, $colorsDef, $colorBackground) {
    $tile = imagecreatetruecolor($width, $height);

    # barvy
    $colorsCount = sizeOf($colorsDef);
    for ($i = 0; $i < $colorsCount; $i++) {
        $col[$i] = imagecolorallocate($tile, $colorsDef[$i][0], $colorsDef[$i][1], $colorsDef[$i][2]);
    }
    $colorsCount--;
    if ($colorsCount < 0)
        return;

    $colors = array($col[0], $col[1], $col[2], $col[3], $col[4], $col[5], $col[6], $col[7], $col[8]);

    # tloustka pera
    imagesetthickness($tile, $thicknes);

    # barva pozadi
    $background = imagecolorallocate($tile, $colorBackground[0], $colorBackground[1], $colorBackground[2]);
    imagefill($tile, 0, 0, $background);

    # nejvetsi kruznice
    if ($shownShapes[0]) {
        circle($tile, array(1/2, 1/2), 0, 8, 1, $width, $height, $colormap[0], $colors);
    }

    # velke puloblouky 1
    if ($shownShapes[1]) {
        circle($tile, array(1, 1/2), 90, 4, 1, $width, $height, $colormap[1], $colors);
    }

    # velke puloblouky 2
    if ($shownShapes[2]) {
        circle($tile, array(1/2, 1), 180, 4, 1, $width, $height, $colormap[2], $colors);
    }

    # velke puloblouky 3
    if ($shownShapes[3])
        circle($tile, array(0, 1/2), 270, 4, 1, $width, $height, $colormap[3], $colors);

    # velke puloblouky 4
    if ($shownShapes[4]) {
        circle($tile, array(1/2, 0), 0, 4, 1, $width, $height, $colormap[4], $colors);
    }

    # nejvetsi ctverec 45
    if ($shownShapes[5]) {
        $points = array(1, 0.5, 0.75, 0.75, 0.5, 1, 0.25, 0.75, 0, 0.5, 0.25, 0.25, 0.5, 0, 0.75, 0.25);
        square($tile, $points, $width, $height, $colormap[5], $colors);
    }

    # mala kruznice
    if ($shownShapes[6]) {
        circle($tile, array(1/2, 1/2), 0, 8, sqrt(2)/2, $width, $height, $colormap[6], $colors);
    }

    # druhy nejvetsi ctverec 90
    if ($shownShapes[7]) {
        $points = array(3/4, 1/2, 3/4, 3/4, 1/2, 3/4, 1/4, 3/4, 1/4, 1/2, 1/4, 1/4, 1/2, 1/4, 3/4, 1/4);
        square($tile, $points, $width, $height, $colormap[7], $colors);
    }

    # maly ctverec 45
    if ($shownShapes[8]) {
        $points = array(3/4, 1/2, 5/8, 5/8, 1/2, 3/4, 3/8, 5/8, 1/4, 1/2, 3/8, 3/8, 1/2, 1/4, 5/8, 3/8);
        square($tile, $points, $width, $height, $colormap[8], $colors);
    }

    #$points = array(0, 0, 100, 100, 60, 20, 30, 60);
    #imagepolygon($tile, $points, 4, IMG_COLOR_STYLED);

    return $tile;
}


# prida do dlazdice ctverec
function square($tile, $points, $width, $height, $colormap, $colors) {
    for ($i = 0; $i < sizeOf($points)/2; $i += 1) {
        if ($colormap[$i])
            imageline($tile, $width*$points[2*$i], $height*$points[2*$i+1], $width*$points[(2*$i+2)%sizeOf($points)], $height*$points[(2*$i+3)%sizeOf($points)], $colors[$colormap[$i]-1]);
    }
}


# prida do dlazdice oblouk
function circle($tile, $center, $degree, $parts, $d, $width, $height, $colormap, $colors) {
    for ($i = 0; $i < $parts; $i += 1) {
        if ($colormap[$i])
            imagearc($tile, $width*$center[0], $height*$center[1], $width*$d, $height*$d, ($i*45 + $degree)%360, ($i*45 + $degree + 45)%360, $colors[$colormap[$i]-1]);
    }
}


# generuje vysledny obrazek
function generateImg($width, $height, $tile, $transform, $tilesCount) {
    $img  = imagecreatetruecolor($width*$tilesCount[0], $height*$tilesCount[1]);

    if ($transform[0] && $transform[1]) {
        $tileBigHalf = imagecreatetruecolor($width*2, $height);
        imagecopy($tileBigHalf, $tile, 0, 0, 0, 0, $width, $height); 
        imageflip($tileBigHalf, IMG_FLIP_HORIZONTAL);
        imagecopy($tileBigHalf, $tile, 0, 0, 0, 0, $width, $height); 

        $tileBig = imagecreatetruecolor($width*2, $height*2);
        imagecopy($tileBig, $tileBigHalf, 0, 0, 0, 0, $width*2, $height); 
        imageflip($tileBig, IMG_FLIP_VERTICAL);
        imagecopy($tileBig, $tileBigHalf, 0, 0, 0, 0, $width*2, $height); 
    
    } else if ($transform[0]) {
        $tileBig = imagecreatetruecolor($width*2, $height);
        imagecopy($tileBig, $tile, 0, 0, 0, 0, $width, $height); 
        imageflip($tileBig, IMG_FLIP_HORIZONTAL);
        imagecopy($tileBig, $tile, 0, 0, 0, 0, $width, $height); 

    } else if ($transform[1]) {
        $tileBig = imagecreatetruecolor($width, $height*2);
        imagecopy($tileBig, $tile, 0, 0, 0, 0, $width, $height); 
        imageflip($tileBig, IMG_FLIP_VERTICAL);
        imagecopy($tileBig, $tile, 0, 0, 0, 0, $width, $height); 

    } else {
        $tileBig = imagecreatetruecolor($width, $height);
        imagecopy($tileBig, $tile, 0, 0, 0, 0, $width, $height); 
    }

    imagesettile($img, $tileBig);
    imagefilledrectangle($img, 0, 0, $width*$tilesCount[0], $height*$tilesCount[1], IMG_COLOR_TILED);

    return $img;
}


