<?php
?>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html"; charset="utf-8">
    <title>Návod - Tiles</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

<div id="tutorial">
<div id="header">
<h1>Tiles - Návod</h1>
<p id="authors">autoři: Václav Stránský a Hana Brychtová</p>
   <a class="button" href="index.php">Zpět</a>

<div class="images"></div>

</div>

<div>
<div class="section">
<h3>Menu</h3>
<table>
<tr><th>Default settings</th><td>nastaví dlaždici do ukázkového nastavení</td></tr>
<tr><th>Galery</th><td>přejde na stránku s ukázkovými obrázky</td><tr>
<tr><th>CZ Tutorial</th><td>přejde na stránku s tímto návodem</td></tr>
</table>
</div>

<div class="section">
<h3>Uložení</h3>
<table>
<tr><th>Uložení celé mozaiky</th><td>Pravým tlačítkem myši klikněte na mozaiku a zvolte "Uložit obrázek jako"</td></tr>
<tr><th>Uložení jedné dlaždice</th><td>Nastavte počet dlaždic v řádku a sloupci na 1 nebo 2 (podle symetrie) a přegenerujte, dále postupujte jako u celé mozaiky</td></tr>
</table>
</div>

<div class="section">
<h3>Tlačítka nastavení</h3>
<table>
<tr><th>Randomize</th><td>vygeneruje náhodnou dlaždici na základě uživatelova nastavení</td></tr>
<tr><th>Redraw</th><td>překreslí změněné uživatelské nastavení</td></tr>
</table>
</div>

<div class="section">
<h3>Nastavení dlaždice</h3>
<table>
<tr><th>Horizontal symetry</th><td>zapne nebo vypne horizontální překlopení</td></tr>
<tr><th>Vertical symetry</th><td>zapne nebo vypne vertikální překlopení</td></tr>
<tr><th>Line thicknes</th><td>nastaví šířku čáry</td></tr>
<tr><th>Tile Width</th><td>šířka jedné dlaždice</td></tr>
<tr><th>Tile Height</th><td>výška jedné dlaždice</td></tr>
<tr><th>Tiles in a column</th><td>počet dlaždic ve sloupci</td></tr>
<tr><th>Tiles in a row</th><td>počet dlaždic na řádku</td></tr>
<tr><th>Background Color</th><td>barva pozadí</td></tr>
<tr><th>Line colors</th><td>barvy čar, tlačítkem Add se přidá další barva, tlačítkem remove se odstraní, lze mít 1 - 9 barev</td></tr>
<tr><th>Line density</th><td>Nastaví pravděpodobnost, že se čára přidá do matice, použije se při náhodném generování</td></tr>
<tr><th>Select displayed shapes</th><td>Zatržením daného tvaru se budou jeho části používat ke generování dlaždice</td></tr>

</table>
</body>
</html> 
