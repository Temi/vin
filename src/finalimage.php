<?php
include 'tile.php';

# sirka a vyska dlazdice
$width = 100;
$height = 100;

$colors = array();

# barva pozadi
$colorbackground = array(50, 10, 0);

# sirka car
$thicknes = 5;

# flip dlazdice (horizontal, vertical)
$transform = array(1, 1);

# pocet dlazdic v obrazku (radek, sloupec)
$tilesCount = array(10, 10);

$shownShapes = array(1, 1, 1, 1, 1, 1, 1, 1, 1);
# $shownShapes = array(1, 0, 0, 0, 0, 1, 0, 1, 1);

# mira viditelnosti car (od 0 do 100)
$visibilityRate = 30;

$colormap = array(
    array(0,0,0,0,0,0,0,0),
    array(0,0,0,0,0,0,0,0),
    array(0,0,0,0,0,0,0,0),
    array(0,0,0,0,0,0,0,0),
    array(0,0,0,0,0,0,0,0),
    array(0,0,0,0,0,0,0,0),
    array(0,0,0,0,0,0,0,0),
    array(0,0,0,0,0,0,0,0),
    array(0,0,0,0,0,0,0,0)
);


# zda se ma pregenerovat dlazdice
$regenerate = 1;


function update ($thick,$visRate,$hsymetry,$vsymetry,$RGB,$new_colormap,
		  $tileW,$tileH,$canvasW,$canvasH,
		  $c1,$c2,$c3,$c4,$c5,$c6,$c7,$c8,$c9,
		  $s1,$s2,$s3,$s4,$s5,$s6,$s7,$s8,$s9) {
		  
    global $thicknes;
    global $transform;
    global $tilesCount;
    global $shownShapes;
    global $visibilityRate;
    global $regenerate;
    global $colorbackground;
    global $colormap;
    global $colors;
    
    global $width;
    global $height;
    global $tilesCount; 
    global $regenerate;
  
    $width = $tileW;
    $height= $tileH;
    $tilesCount[0] = $canvasW;
    $tilesCount[1] = $canvasH;

    $thicknes = $thick;
    $visibilityRate = $visRate;
    $transform[0] = $hsymetry;
    $transform[1] = $vsymetry;
    $colorbackground = hex2rgb($RGB);

    for ($j = 0; $j < 9; $j++) {
        for ($i = 0; $i < 8; $i++) {
            $colormap[$j][$i] = $new_colormap[$j*8 + $i];
        }
    }

    $colors = array(hex2rgb($c1), hex2rgb($c2), hex2rgb($c3), hex2rgb($c4), hex2rgb($c5), hex2rgb($c6), hex2rgb($c7), hex2rgb($c8), hex2rgb($c9));
    $shownShapes = array($s1, $s2, $s3, $s4, $s5, $s6, $s7, $s8, $s9);
}


function getTile() {
    global $width;
    global $height;
    global $colors;
    global $colorbackground;
    global $thicknes;
    global $shownShapes;
    global $visibilityRate;
    global $colormap;
    
    #global $tile;

    $tile = generateTile($width, $height, $shownShapes, $colormap, $thicknes, $colors, $colorbackground);
    return $tile;
}


function getFinalImg() {
    global $width;
    global $height;
    global $colors;
    global $colorbackground;
    global $thicknes;
    global $transform;
    global $tilesCount;
    global $shownShapes;
    global $visibilityRate;
    global $regenerate;
    
    global $tile;

    if ($regenerate) {
        $tile = getTile();
    }

    $img = generateImg($width, $height, $tile, $transform, $tilesCount);

    return $img;
}


function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   $rgb = array($r, $g, $b);
   return $rgb;
}

