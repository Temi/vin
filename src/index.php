<?php
require "params.php";
?>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html"; charset="utf-8">
    <title>Tiles</title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

<div id="wrapper">
<div id="header">
<h1>Tiles</h1>
<p id="authors">by Václav Stránský and Hana Brychtová</p>
</div>

<div class="float">
  <div id="control">
  <div class="menu section">
    <a class="button text" href="index.php">Default settings</a>
    <a class="button text" href="galery.php">Galery</a>
    <a class="button text" href="tutorial.php">CZ Tutorial</a>
  </div>
  <div class="section">
    <form action="index.php" method="GET">
      <h3>Settings</h3>
      <table>
      <tr><td class="centering"><input class="button" type="submit" name="submit" value="Randomize" onclick="generateRandom()"/></td> <td class="centering">
      <input class="button" type="submit" name="submit" value="Redraw" onclick="viewColors()"/></td></tr>
      
      
      <tr class="odd"><td class="text">Horizontal symetry</td> <td><input type="checkbox" name="hsymetry" value="true"<?php echo (!empty($_GET['hsymetry']) || empty($_GET['colormap'])) ? " checked" : ""; ?>></td></tr>
      <tr><td class="text">Vertical symetry</td> <td><input type="checkbox" name="vsymetry" value="true"<?php echo (!empty($_GET['vsymetry']) || empty($_GET['colormap'])) ? " checked" : ""; ?>></td></tr>
      <tr class="odd"><td class="text">Line thicknes</td> <td><input class="med" type="range" min="0" max="100" name="thick" value="<?php echo $PARAMS['thick']; ?>"></td></tr>
      <input id="colormap_input" type="hidden" name="colormap" value="<?php echo $PARAMS['colormap']; ?>" />
      <input id="colors_count" type="hidden" name="colors_count" value="<?php echo $PARAMS['colors_count']; ?>" />



      <tr><td class="text">Tile Width</td> <td><input class="smaller" type="number" min="1" max="1000" name="tileW" value="<?php echo $PARAMS['tileW']; ?>"></td></tr>
      <tr class="odd"><td class="text">Tile Height</td> <td><input class="smaller" type="number" min="1" max="1000" name="tileH" value="<?php echo $PARAMS['tileH']; ?>"></td></tr>
      <tr><td class="text">Tiles in a column</td> <td><input class="smaller" type="number" min="1" max="1000" name="canvasH" value="<?php echo $PARAMS['canvasH']; ?>"></td></tr>
      <tr class="odd"><td class="text">Tiles in a row</td> <td><input class="smaller" type="number" min="1" max="1000" name="canvasW" value="<?php echo $PARAMS['canvasW']; ?>"></td></tr>
      <tr><td class="text">Background Color</td> <td><input name="bgcolor" value="<?php echo $PARAMS['bgcolor']; ?>" type="color"></td></tr>
      <tr class="odd"><td class="text">Line colors</td> <td id = "colors">
      <span id="color_span0" class="color_span">
          <input id="color_0" name="color[]" value="<?php echo $PARAMS['color1']; ?>" type="color" /><input class="button2" type="button" value="Add" onclick="addColor(0)"/>
      </span>
      </td></tr>
       
      <tr><td id="up" class="text">Line density</td> <td><input class="med" type="range" min="0" max="100" name="visRate" value="<?php echo $PARAMS['visRate']; ?>"> </td></tr>  
      <tr><td id="small">(for Randomize only)</td> <td></td></tr>
     
     <tr class="odd">
     <td colspan="2" class="text shapes">Select displayed shapes <br />
      <span id="selective1" class="visible"><img src="../shapes/1.png"><br />1<input type="checkbox" name="shape1" value="true"<?php echo (!empty($_GET['shape1']) || empty($_GET['colormap'])) ? " checked" : ""; ?>></span>
      <span id="selective2" class="visible"><img src="../shapes/2.png"><br />2<input type="checkbox" name="shape2" value="true"<?php echo (!empty($_GET['shape2']) || empty($_GET['colormap'])) ? " checked" : ""; ?>></span>
      <span id="selective3" class="visible"><img src="../shapes/3.png"><br />3<input type="checkbox" name="shape3" value="true"<?php echo (!empty($_GET['shape3']) || empty($_GET['colormap'])) ? " checked" : ""; ?>></span>
      <span id="selective4" class="visible"><img src="../shapes/4.png"><br />4<input type="checkbox" name="shape4" value="true"<?php echo (!empty($_GET['shape4'])) ? " checked" : ""; ?>></span>
      <span id="selective5" class="visible"><img src="../shapes/5.png"><br />5<input type="checkbox" name="shape5" value="true"<?php echo (!empty($_GET['shape5']) || empty($_GET['colormap'])) ? " checked" : ""; ?>></span>
      <span id="selective6" class="visible"><img src="../shapes/6.png"><br />6<input type="checkbox" name="shape6" value="true"<?php echo (!empty($_GET['shape6'])) ? " checked" : ""; ?>></span>
      <span id="selective7" class="visible"><img src="../shapes/7.png"><br />7<input type="checkbox" name="shape7" value="true"<?php echo (!empty($_GET['shape7'])) ? " checked" : ""; ?>></span>
      <span id="selective8" class="visible"><img src="../shapes/8.png"><br />8<input type="checkbox" name="shape8" value="true"<?php echo (!empty($_GET['shape8'])) ? " checked" : ""; ?>></span>
      <span id="selective9" class="visible"><img src="../shapes/9.png"><br />9<input type="checkbox" name="shape9" value="true"<?php echo (!empty($_GET['shape9']) || empty($_GET['colormap'])) ? " checked" : ""; ?>></span>
      </td>
    
     </tr>
     
    
       </table>

       <script>document.write('<div class="fb-share-button" data-href="" data-layout="button"></div>');</script>
      
       <script>
       function getQueryVariable(idx)
       {
           var query = window.location.search.substring(1);
           var vars = query.split("&");
           var col;
           var cNum = 0;
           var defaultColor = ["#ff8100", "#63380c", "#ffb600", "#fff0e1"];
  
  
           for (var i=0;i<vars.length;i++) {
                   var pair = vars[i].split("=");
                   if(pair[0] == "color[]" || pair[0] == "color%5B%5D"){
                        col = pair[1];

                        if (cNum >= idx) {
                            break;
                        }
                        
                        cNum++
                   }
           }
           if (col) {
               col = col.replace("%23", "#");
           } else {
               col = defaultColor[idx];
           }

           return(col);
        }
      
        var elColorsCount = document.getElementById("colors_count");
        var colorsCount = elColorsCount.value;

        var color0 = document.getElementById("color_0");
        color0.value = getQueryVariable(0);


        for (var i=1; i < colorsCount; i++) {
            addColor(i);
            elColorsCount.value = elColorsCount.value - 1;
        }

      
        function addColor(id) {
          var elColorsCount = document.getElementById("colors_count");
          var colorsCount = elColorsCount.value;
          if (colorsCount < 9)
          {
            colorsCount++;
            elColorsCount.value = colorsCount;
            if (id == 0) {
                id = colorsCount;
            }
            var colorVal = getQueryVariable(id);

            var colorBtn = document.createElement("input");
            colorBtn.id = "color_"+id;
            colorBtn.type = "color";
            colorBtn.name = "color[]";
            colorBtn.value = colorVal;

            var removeBtn = document.createElement("input");
            removeBtn.className = "button2";
            removeBtn.type = "button";
            removeBtn.value = "remove";
            removeBtn.onclick = function() {
                var colors = document.getElementById("colors");
                var colorSpan = document.getElementById(this.parentNode.id);
                colorSpan.parentNode.removeChild(colorSpan);   
                elColorsCount.value = elColorsCount.value - 1;
            };

            elColorsCount.value = colorsCount;
            var colorSpan = document.createElement("span");
            colorSpan.id = "color_span" + id;
            colorSpan.className = "color_span";

            colorSpan.appendChild(colorBtn);
            colorSpan.appendChild(removeBtn);

            var colors = document.getElementById('colors');
            colors.appendChild(colorSpan);
          }
        }
	
        function generateRandom() {
            var val = "";
            var visRate = document.getElementsByName("visRate")[0].value / 100;
            var elColorsCount = document.getElementById("colors_count");
            var colorsCount = elColorsCount.value;

            for (j = 0; j < 9; j++) {
                for (i = 0; i < 8; i++) {
                    if (Math.random() < visRate) {
                        val += Math.floor((Math.random() * colorsCount + 1));
                    } else 
                        val += 0;
                }
            }
            var el = document.getElementById("colormap_input");
            el.value = val;
        }

      </script>
      
  </form>
  </div>

  </div>
</div>


<div class="float">
  <div id="fullimgdiv">
    <img src="showfinalimage.php?<?php echo http_build_query($PARAMS); ?>" id="fullimg" />
  </div>
</div>

<div id="fb-root"></div>
<script>
var selected = 9;


function backS() {
var previous;
  if (selected == 1)
  {
    selected = 9;
    previous = 1;
    }
  else
  {
    previous = selected;
    selected = selected - 1;
    }
    
    document.getElementById("selective"+selected).className = "visible";
    document.getElementById("selective"+previous).className = "unvisible";
  
}

function nextS() {
var previous;
  if (selected == 9)
  {
    selected = 1;
    previous = 9;
    }
  else
  {
    previous = selected;
    selected = selected + 1;
    }
    
    document.getElementById("selective"+selected).className = "visible";
    document.getElementById("selective"+previous).className = "unvisible";

}


</script>

<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/us_EN/sdk.js#xfbml=1&version=v2.3";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


</div>
</body>
</html> 
