<?php



# parametry ke zmene rozmeru
if (!empty($_GET['tileW']))
  $PARAMS['tileW'] = $_GET['tileW'];
else
  $PARAMS['tileW'] = 100;
  
if (!empty($_GET['tileH']))
  $PARAMS['tileH'] = $_GET['tileH'];
else
  $PARAMS['tileH'] = 100;

if (!empty($_GET['canvasH']))
  $PARAMS['canvasH'] = $_GET['canvasH'];
else
  $PARAMS['canvasH'] = 10;
  
if (!empty($_GET['canvasW']))
  $PARAMS['canvasW'] = $_GET['canvasW'];
else
  $PARAMS['canvasW'] = 10;
  
  
# parametry ke generovani 
if (!empty($_GET['thick']))
  $PARAMS['thick'] = $_GET['thick'];
else
  $PARAMS['thick'] = 4;
  
if (!empty($_GET['visRate']))
  $PARAMS['visRate'] = $_GET['visRate'];
else
  $PARAMS['visRate'] = 30;
  
if (!empty($_GET['hsymetry']))
  $PARAMS['hsymetry'] = $_GET['hsymetry'];
else if (empty($_GET['colormap']))
  $PARAMS['hsymetry'] = true;
else
  $PARAMS['hsymetry'] = false;
  
if (!empty($_GET['vsymetry']))
  $PARAMS['vsymetry'] = $_GET['vsymetry'];
else if (empty($_GET['colormap']))
  $PARAMS['vsymetry'] = true;
else
  $PARAMS['vsymetry'] = false;
  
  # BG 
if (!empty($_GET['bgcolor']))
  $PARAMS['bgcolor'] = $_GET['bgcolor'];
else
  $PARAMS['bgcolor'] = "#121212";

if (!empty($_GET['colormap']))
  $PARAMS['colormap'] = $_GET['colormap'];
else
  $PARAMS['colormap'] = "030030004200000000311003000000000004200000200003400020400000000002040000";
  
# barvy
if (!empty($_GET['colors_count']))
  $PARAMS['colors_count'] = $_GET['colors_count'];
else
  $PARAMS['colors_count'] = 4;

#preposle vsechny barvy, nevyplnene jako prazdne
if (!empty($_GET['color']))
{
  $i = 1;
  foreach ($_GET['color'] as $addColor)
  {
    $PARAMS['color'.$i] = $addColor;
    $i++;
  }
  for ($i;$i<=9;$i++)
    $PARAMS['color'.$i] = "";

}
else
{
  #defaultni barva
  $PARAMS['color1'] = "#ff8100";
  $PARAMS['color2'] = "#63380c";
  $PARAMS['color3'] = "#ffb600";
  $PARAMS['color4'] = "#fff0e1";
  $PARAMS['color5'] = "";
  $PARAMS['color6'] = "";
  $PARAMS['color7'] = "";
  $PARAMS['color8'] = "";
  $PARAMS['color9'] = "";
}
 
 
 
#tvary
if (!empty($_GET['shape1']))
  $PARAMS['shape1'] = $_GET['shape1'];
else if (empty($_GET['colormap']))
  $PARAMS['shape1'] = true;
else
  $PARAMS['shape1'] = false;
  
if (!empty($_GET['shape2']))
  $PARAMS['shape2'] = $_GET['shape2'];
else if (empty($_GET['colormap']))
  $PARAMS['shape2'] = true;
else
  $PARAMS['shape2'] = false;

if (!empty($_GET['shape3']))
  $PARAMS['shape3'] = $_GET['shape3'];
else if (empty($_GET['colormap']))
  $PARAMS['shape3'] = true;
else
  $PARAMS['shape3'] = false;
  
if (!empty($_GET['shape4']))
  $PARAMS['shape4'] = $_GET['shape4'];
else
  $PARAMS['shape4'] = false;
  
if (!empty($_GET['shape5']))
  $PARAMS['shape5'] = $_GET['shape5'];
else if (empty($_GET['colormap']))
  $PARAMS['shape5'] = true;
else
  $PARAMS['shape5'] = false;

if (!empty($_GET['shape6']))
  $PARAMS['shape6'] = $_GET['shape6'];
else
  $PARAMS['shape6'] = false;

if (!empty($_GET['shape7']))
  $PARAMS['shape7'] = $_GET['shape7'];
else
  $PARAMS['shape7'] = false;

if (!empty($_GET['shape8']))
  $PARAMS['shape8'] = $_GET['shape8'];
else
  $PARAMS['shape8'] = false;
 
if (!empty($_GET['shape9']))
  $PARAMS['shape9'] = $_GET['shape9'];
else if (empty($_GET['colormap']))
  $PARAMS['shape9'] = true;
else
  $PARAMS['shape9'] = false;
  
  
  
  
# tlacitka
if (!empty($_GET['submit']))
{
  switch($_GET['submit'])
  {
  case 'Randomize':  
    $PARAMS['submit'] = 'Randomize';
    break;
  case 'Redraw':
    $PARAMS['submit'] = 'Redraw';
    break;
  default:
    $PARAMS['submit'] = false;
    break;
  }
}
else
  $PARAMS['submit'] = false;
